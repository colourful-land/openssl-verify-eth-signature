<?php

require 'vendor/autoload.php';


require 'src/Verifier.php';

$pem = "-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEljjcCgCUScCYxLxEeoT5hkeS2d1MMoCg
1/DS83rpU6Fzu7Rm9BGL4NNmRQwHg0AR+biiq/N531n1uSVQ+YVtAw==
-----END PUBLIC KEY-----";

$message = "test";
$signature = '396cd2881fdc8e1898f919e275aa44ff50565c1608e6d54db41c628ba253058c1deb5a41af8f9bba1d301d22587072cf47cf93ea7d69bd87094bcd5f1ee6bb021c';


$verified = \Lynn\EthereumSignature\Verifier::verify( $message, $signature, $pem );
echo "this should print 1:
$verified
";
