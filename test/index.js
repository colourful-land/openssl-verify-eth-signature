const EthCrypto = require('eth-crypto');
const Su = require('eth-sig-util')
const EJS = require('ethereumjs-util')
const Encoder = require('key-encoder').default
const encoder = new Encoder('secp256k1')
// const hex2bin = require('hex-to-binary')
// const Personal = require('web3-eth-personal')
// const p = new Personal
/**
 * This file has some stuff for testing.
 */

const private = '' // omitted

const message = 'test'
const messageHex = '0x74657374'
const signature = '0x396cd2881fdc8e1898f919e275aa44ff50565c1608e6d54db41c628ba253058c1deb5a41af8f9bba1d301d22587072cf47cf93ea7d69bd87094bcd5f1ee6bb021c'
const signer1 = Su.recoverPersonalSignature({
	sig: signature,
	data: (message)
})
const signer2 = EthCrypto.recover(
	signature,
	EthCrypto.hash.keccak256(message)
)
const _res = EJS.fromRpcSig(signature)

const hashedMessage = EJS.hashPersonalMessage(Buffer.from(message))

const signer3 = EJS.ecrecover(hashedMessage, _res.v, _res.r, _res.s)

// const signing = EJS.ecsign(hashedMessage, EJS.toBuffer( '0x' + private))
// const x = Su.personalSign( EJS.toBuffer('0x'+private), { data: message.toString('hex') } )

// console.log('RES!!!!!!!!', _res, signing, 'xxx', x, '??', Buffer.from(x.slice(2), 'hex').toString('base64'), 'msg', hashedMessage.toString('base64'))
// console.log('HASHHHHHHHHH!!!!!!!!', EJS.hashPersonalMessage(Buffer.from(message)).toString('hex'))

const pubkey1 = Su.extractPublicKey({
	sig: signature,
	data: message
})

const pubkey2 = EthCrypto.recoverPublicKey(
	signature,
	EthCrypto.hash.keccak256(message)
)
const s = '04' + signer3.toString('hex')
// console.log(('04' + s).length)

// console.log(s, '\n', EJS.pubToAddress(signer3).toString('hex'))
console.log(encoder.encodePublic(s, 'raw', 'pem'))
// console.log('really?', encoder.encodePrivate( private, 'raw', 'pem'))
// const k = signer3.toString()
// console.log('huh?', signer1, '\n 2 ', signer2, '\n3', signer3.toString('hex'), '\nencoded', encoder.encodePublic(pubkey1, 'raw', 'pem'))
// console.log('\npk1', pubkey1, '\npk2', pubkey2)

