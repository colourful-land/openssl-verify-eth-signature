<?php
/**
 * Verifies Ethereum signatures using EllipticCurve library which uses openssl.
 *
 * This is the ECDSA lib: https://github.com/starkbank/ecdsa-php .
 *
 * PHP version 5.6 (or 5.5?)
 *
 * @category Dunno
 * @package  EthereumSignature
 * @author   lynn <example@example.example>
 * @license  Unlicense <https://unlicense.org>
 * @link     https://gitlab.org/losnappas/openssl-verify-eth-signature
 * @since    0.1.0
 */

namespace Lynn\EthereumSignature;

require_once 'vendor/starkbank/ecdsa/src/ellipticcurve.php';
use Keccak\Keccak256;

/**
 * Static methods for signature verifications.
 *
 * @category Dunno
 * @package  EthereumSignature
 * @author   lynn <example@example.example>
 * @license  Unlicense <https://unlicense.org>
 * @link     https://gitlab.org/losnappas/openssl-verify-eth-signature
 * @since    0.1.0
 */
class Verifier
{

    /**
     * Gets ethereum personal_sign message prefix.
     *
     * @param string $message The message.
     *
     * @since 0.1.0
     *
     * @return strin Prefix for message.
     */
    private static function _getPrefix( $message )
    {
        $messageLen = strlen($message);
        $prefix     = json_decode('"\u0019"') .
        "Ethereum Signed Message:\n{$messageLen}";
        return $prefix;
    }

    /**
     * Hashes message for signing.
     *
     * @param string $message Message to hash.
     *
     * @since 0.1.0
     *
     * @return string The hash.
     */
    private static function _hash( $message )
    {
        $prefix         = self::_getPrefix($message);
        $messageHashRaw = Keccak256::hash($prefix . $message, 256);
        $messageHash    = base64_encode(hex2bin($messageHashRaw));
        return $messageHash;
    }

    /**
     * Verifies message signatures.
     *
     * @param mixed  $message      Message that was signed.
     * @param string $signature    Signature in hex.
     * @param string $publicKeyPem Public key that signed, in PEM format.
     *
     * @since 0.1.0
     *
     * @return boolean True if signature matches, false otherwise.
     */
    public static function verify( $message, $signature, $publicKeyPem )
    {
        $messageHash = self::_hash($message);
        $signature   = base64_encode(hex2bin($signature));
        // var_dump($signature, $messageHash);
        $signature   = \EllipticCurve\Signature::fromBase64($signature);
        $publicKey   = \EllipticCurve\PublicKey::fromPem($publicKeyPem);
        // var_dump($publicKey);

        // *** Testing stuff ***
        // Clear errors.
        while ($x = openssl_error_string());
        $openssl_version = openssl_verify(
            $messageHash,
            $signature->toDer(),
            $publicKey->openSslPublicKey,
            OPENSSL_ALGO_SHA256
        );
        var_dump($openssl_version);
        while ($x = openssl_error_string()) {
                echo "openssl : $x\n";
        }
        // *** /Testing stuff ***

        $verified    = \EllipticCurve\Ecdsa::verify(
            $messageHash,
            $signature,
            $publicKey
        );
        return $verified;
    }
}
